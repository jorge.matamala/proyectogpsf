<?php require_once "menu.php"; ?>
<?php
include("conexion3.php");




function generarCodigo($longitud) {
 $key = '';
 $pattern = '123456789';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
 
 $cod_mascota=$_GET['cod_mascota'];


      
          $consulta1 = mysqli_query ($con,"SELECT * FROM mascota where cod_mascota='$cod_mascota'");
            $mostrar=mysqli_fetch_array($consulta1)
        ?>



<!DOCTYPE HTML>
<html lang="en">

<head>
 <title>Nuevo ficha medica</title>
  
</head>
<body>
   <link rel="stylesheet" type="text/css" href="estilossss.css">

  <div class="encabezado">  
          <h1>Nueva ficha medica</h1>
  </div>

    
<form    class="was-validated" enctype="multipart/form-data" method="POST"  type="hidden"  >
  <div class="contenedor3">
    <div class="form-row">
    
      <div class="col-md-4">
        <label for="cod">codigo de la ficha medica :</label>
       <input type="text" value="<?php echo generarCodigo(4) ?>"  name="cod_ficha"   readonly>
       
      </div>
         <div class="col-md-4">
        <label for="cod">Nombre mascota  :</label>
       <input type="text" value="<?php echo $mostrar['nombre_m']?>"  name="nombre_m"   readonly>
       
      </div>

       <div class="col-md-4">
        <label for="cod">codigo mascota  :</label>
       <input type="text" value="<?php echo $mostrar['cod_mascota']?>"  name="cod_mascota"   readonly>
       
      </div>

      <div class="col-md-4">
        <label for="descripcion">descripcion:</label>
        <input  type="text" class="form-control" id="descripcion" placeholder="Ingrese la descripcion" name="descripcion" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-4">
        <label for="fecha">fecha:</label>
        <input type="date" class="form-control" id="fecha" placeholder="Ingrese fecha" name="fecha" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    

   
   <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
</div>

</form>


 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
 <script>
        $(document).ready(function(){
            $('form').submit(function(event){
                var formData = {
                    'cod_ficha': $('input[name=cod_ficha]').val(),
                   'cod_mascota': $('input[name=cod_mascota]').val(),
                   'descripcion': $('input[name=descripcion]').val(),
                   'fecha': $('input[name=fecha]').val()
                   
                };

                $.ajax({
                    type: 'POST',
                    url: 'ingresarficham.php',
                    data: formData,
                    dataType: 'json',
                    encode: true
                })
                .done(function(data){
                    if(!data.success){
                        if(data.errors.nombre){
                            Swal.fire({
                                icon: 'error',
                                title: 'Ha ocurrido un error',
                                text: data.errors.nombre
                            })
                        }
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'Registro exitoso',
                            text: "La marca se ha registrado con éxito",

                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        }).then((result) => {
                            if (result.value) {
                                location.href = "verfichamedica.php?cod_mascota=<?php echo $mostrar['cod_mascota'] ?>";
                            }
                        })
                    }
                })
                .fail(function(data){
                    console.log(data);
                });
                event.preventDefault();
            });
        });
    </script>

  
</body>
</html>