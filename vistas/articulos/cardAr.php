<?php 
	
	require_once "../../clases/Conexion.php";
	$c= new conectar();
	$conexion=$c->conexion();
	$sql="SELECT art.nombre,
					art.descripcion,
					art.cantidad,
					art.precio,
					img.ruta,
					cat.nombreCategoria,
					art.id_producto
		  from Articulosbd as art 
		  inner join Imagenesbd as img
		  on art.id_imagen=img.id_imagen
		  inner join Categoriasbd as cat
		  on art.id_categoria=cat.id_categoria";
	$result=mysqli_query($conexion,$sql);

 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require_once "../menu.php"; ?>
</head>
<body>


<div class="card" style="width: 18rem; text-align: center">

	<caption><label>Articulos</label></caption><p></p>
	<tr>
	
	</tr>

	<?php while($ver=mysqli_fetch_row($result)): ?>

	<tr><p></p>
		<td><?php echo $ver[0]; ?></td>
		<p></p>
		<td>
			<?php 
			$imgVer=explode("/", $ver[4]) ; 
			$imgruta=$imgVer[1]."/".$imgVer[2]."/".$imgVer[3];
			?>
			<img width="150" height="150" src="<?php echo $imgruta ?>">
		</td>
		
		<p></p>
		<td><?php echo $ver[5]; ?></td>
		<p></p>
		<td><?php echo $ver[1]; ?></td>
		<p></p>
		<td><?php echo $ver[2]; ?></td>
		<p></p>
		<td><?php echo $ver[3]; ?></td>

		
	</tr>
<?php endwhile; ?>
</body>
</html>


</div>